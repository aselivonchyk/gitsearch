import SwiftUI
import Combine

final class SearchViewModel: ObservableObject {
    @Published var entries = Entries(total_count: 0, items: [])
    @Published var searchText = ""
    @Published var isSearching = false

    public var hasResults: Bool { entries.total_count > 0 }
    public var hasMoreRows: Bool { entries.total_count > entries.items.count }

    private let perPage = Int64(30)
    private let maxSearchLength = 256
    private var nextPage: Int64 { (Int64(entries.items.count) + perPage) / perPage }
    private var cancellables = Set<AnyCancellable>()

    private func fetchUrl(for page: Int64) throws -> URL {
        guard let url = URL(string: "https://api.github.com/search/commits?accept=application/vnd.github.v3+json&per_page=\(perPage)&page=\(page)&q=\(searchText)") else {
            throw NSError()
        }
        return url
    }

    public func resetSearch() {
        entries = Entries(total_count: 0, items: [])
        searchText = ""
        isSearching = false
    }

    public func submitSearch() async throws {
        DispatchQueue.main.async {
            self.isSearching = true
        }
        let (data, _) = try await URLSession.shared.data(from: fetchUrl(for: 1))
        let decoded = try JSONDecoder().decode(Entries.self, from: data)

        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.entries = decoded
            self.isSearching = false
        }
    }

    public func fetchMore() async throws {
        let (data, _) = try await URLSession.shared.data(from: fetchUrl(for: nextPage))
        let decoded = try JSONDecoder().decode(Entries.self, from: data)

        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            var mergedEntries = [Entry]()
            mergedEntries.append(contentsOf: self.entries.items)
            mergedEntries.append(contentsOf: decoded.items)

            self.entries = Entries(total_count: self.entries.total_count, items: mergedEntries)
        }
    }
}
