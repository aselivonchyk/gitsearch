struct Entry: Decodable, Hashable {
    let url: String
    let sha: String
    let repository: Repository
}
