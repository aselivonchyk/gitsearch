struct Entries: Decodable, Hashable {
    let total_count: Int64
    let items: [Entry]
}
