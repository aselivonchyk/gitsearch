struct Repository: Decodable, Hashable {
    let html_url: String
    let name: String
}
