import SwiftUI

struct SearchView: View {
    @StateObject var viewModel = SearchViewModel()

    var body: some View {
        if viewModel.isSearching {
            Text("Searching")
                .font(.headline)
        } else {
            NavigationView {
                List {
                    ForEach(viewModel.entries.items, id: \.self) { entry in
                        Link(entry.repository.name, destination: URL(string: entry.repository.html_url)!)
                    }
                    if viewModel.hasMoreRows {
                        Text("Fetching more...")
                            .onAppear(perform: {
                                Task {
                                    try? await viewModel.fetchMore()
                                }
                            })
                    }
                }
                .searchable(text: $viewModel.searchText)
                .onSubmit(of: .search) {
                    Task {
                        try? await viewModel.submitSearch()
                    }
                }
                .navigationTitle(viewModel.hasResults ? "Results for:\" \(viewModel.searchText)\"" : "Search on Github")
            }
        }
    }
}
